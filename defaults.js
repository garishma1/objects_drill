function defaults(obj,defaultprops){
    if(Object.keys(obj).length>0){
        for(let value1 in obj){
            for(let value2 in defaultprops){
                if(value1===value2 && (obj[value1]===null || obj[value1]===undefined)){
                    obj[value1]=defaultprops[value2]
                }
            }
        }
        return obj
    }else{
        return null
    }
}
module.exports = defaults