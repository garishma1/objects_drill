let invert = require("../invert")

let testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' }

let inverted = invert(testObject)

console.log(inverted)