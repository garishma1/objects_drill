function mapObject(obj,cb){
    if(Object.keys(obj).length>0){
        let mappedObject = {}
        for(let key in obj){
            if(obj.hasOwnProperty((key))){
                 mappedObject[key]=cb(obj[key]);
            }
        }
        return mappedObject  
    }else{
        return null
    }
}

module.exports = mapObject