function pairs(obj){
    if(Object.keys(obj).length>0){
        let pairsArray = []
        for(let key in obj){
            if(obj.hasOwnProperty((key))){
                pairsArray.push([key,obj[key]])
            }
        }
        return pairsArray 
    }else{
        return null
    }
}
module.exports = pairs
