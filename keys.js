function keys(obj){
  if(Object.keys(obj).length>0){
    let objKeys =[]
    for(let elements in obj){
      objKeys.push(elements)
    }
    return objKeys
  }else{
    return null
  }
}
module.exports = keys
