function invert(obj){
    if(Object.keys(obj).length>0){
        let invertedObj = {};
        for (let key in obj) {
            if (obj.hasOwnProperty(key)){
                invertedObj[obj[key]] = key;
            }
        }
        return invertedObj;
    }else{
        return null
    }
}
module.exports = invert